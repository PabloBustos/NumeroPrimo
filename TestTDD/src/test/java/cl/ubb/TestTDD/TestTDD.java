package cl.ubb.TestTDD;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;


public class TestTDD {

	@Test
	public void PilaVaciaRetornarVerdadero() {
		/*arrange*/
		PilaTest pila = new PilaTest();
		int Pila[];
		Pila = new int[2];
		boolean Resultado;
		/*act*/
		Resultado=pila.PilaVacia(Pila);
		/* assert */
		assertThat(Resultado,is(true));
		
	}


	
	@Test
	public void Agregar1RetornarPilanoestaVacia() {
		/*arrange*/
		PilaTest pila = new PilaTest();
		int Pila[];
		Pila = new int[2];
		Pila[0]=1;
		boolean Resultado;
		/*act*/
		Resultado=pila.PilaVacia(Pila);
		/* assert */
		assertThat(Resultado,is(false));
		
	}
	
	@Test
	public void Agregar1y2RetornarPilanoestaVacia() {
		/*arrange*/
		PilaTest pila = new PilaTest();
		int Pila[];
		Pila = new int[2];
		Pila[0]=1;
		Pila[1]=2;
		boolean Resultado;
		/*act*/
		Resultado=pila.PilaVacia(Pila);
		/* assert */
		assertThat(Resultado,is(false));
		
	}
	
	@Test
	public void Agregar1y2RetornarTamaņostack2() {
		/*arrange*/
		PilaTest pila = new PilaTest();
		int Pila[];
		Pila = new int[2];
		Pila[0]=1;
		Pila[1]=2;
		int Resultado;
		/*act*/
		Resultado=pila.PilaStack(Pila);
		/* assert */
		assertEquals(Resultado,2);
		
	}
	
	@Test
	public void Agregar1HacerPopRetornar1() {
		/*arrange*/
		PilaTest pila = new PilaTest();
		int Pila[];
		Pila = new int[2];
		Pila[0]=1;
		Pila[1]=0;
		int Resultado;
		/*act*/
		Resultado=pila.PilaPop(Pila);
		/* assert */
		assertEquals(Resultado,1);
		
	}
	
	@Test
	public void Agregar1y2HacerPopRetornar2() {
		/*arrange*/
		PilaTest pila = new PilaTest();
		int Pila[];
		Pila = new int[2];
		Pila[0]=1;
		Pila[1]=2;
		int Resultado;
		/*act*/
		Resultado=pila.PilaPop(Pila);
		/* assert */
		assertEquals(Resultado,2);
		
	}
	
	@Test
	public void Agregar3y4HacerPop2VecesRetornar4y3() {
		/*arrange*/
		PilaTest pila = new PilaTest();
		int Pila[];
		Pila = new int[2];
		Pila[0]=3;
		Pila[1]=4;
		int Resultado;
		int Resultado2;
		/*act*/
		Resultado=pila.PilaPop(Pila);
		/* assert */
		assertEquals(Resultado,4);
		/*act*/
		Resultado2=pila.PilaPop(Pila);
		/* assert */
		assertEquals(Resultado2,3);
		
	}
}
